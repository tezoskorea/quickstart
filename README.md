# Tezos Quick Build
- Environment: Ubuntu 16.04 LTS, 18.04 LTS (aws, google cloud)

## How to Use
- 1) Mainnet
```
curl "https://gitlab.com/tezoskorea/quickstart/raw/master/tz_install.sh" | bash -s mainnet
```
- 2) Carthagenet
```
curl "https://gitlab.com/tezoskorea/quickstart/raw/master/tz_install.sh" | bash -s carthagenet
```
